# https://gitlab.com/docker666/deepin-apricot

Deepin Minimal Environment in Docker 

```
```

# Usage

Pull Image
```
docker pull registry.gitlab.com/docker666/deepin-apricot:latest
```

Run container in Docker
```
docker run -itd --name deepin-apricot registry.gitlab.com/docker666/deepin-apricot:latest
docker exec -it deepin-apricot /bin/bash
```

Run container DIND
```
docker run -itd --name deepin-apricot -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/docker666/deepin-apricot:latest
```

Run as Base Image
```
FROM registry.gitlab.com/docker666/deepin-apricot:latest
```
